(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/User/list-user/list-user.component.css":
/*!********************************************************!*\
  !*** ./src/app/User/list-user/list-user.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/User/list-user/list-user.component.html":
/*!*********************************************************!*\
  !*** ./src/app/User/list-user/list-user.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "LIST USER\n\n<ul>\n  <li *ngFor=\"let user of users\">\n    <span>{{user.firstName}}</span>\n  </li>\n</ul>\n"

/***/ }),

/***/ "./src/app/User/list-user/list-user.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/User/list-user/list-user.component.ts ***!
  \*******************************************************/
/*! exports provided: ListUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListUserComponent", function() { return ListUserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListUserComponent = /** @class */ (function () {
    function ListUserComponent() {
        this._users = [];
    }
    ListUserComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(ListUserComponent.prototype, "users", {
        get: function () {
            return this._users;
        },
        set: function (value) {
            this._users = value;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], ListUserComponent.prototype, "users", null);
    ListUserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-user',
            template: __webpack_require__(/*! ./list-user.component.html */ "./src/app/User/list-user/list-user.component.html"),
            styles: [__webpack_require__(/*! ./list-user.component.css */ "./src/app/User/list-user/list-user.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ListUserComponent);
    return ListUserComponent;
}());



/***/ }),

/***/ "./src/app/User/user.service.ts":
/*!**************************************!*\
  !*** ./src/app/User/user.service.ts ***!
  \**************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService_1 = UserService;
    UserService.prototype.query = function () {
        return this.http.get(UserService_1.URL_API_USER);
    };
    UserService.prototype.post = function (user) {
        return this.http.post(UserService_1.URL_API_USER, user.toJson());
    };
    UserService.prototype.update = function (user) {
        return this.http.put(UserService_1.URL_API_USER, user.toJson());
    };
    UserService.prototype.delete = function (user) {
        return this.http.delete(UserService_1.URL_API_USER + '/' + user.id);
    };
    var UserService_1;
    UserService.URL_API_USER = "/api/user";
    UserService = UserService_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/User/user.ts":
/*!******************************!*\
  !*** ./src/app/User/user.ts ***!
  \******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(firstName, lastName, email, phoneNumber, postCode, securityLevel, balance) {
        if (firstName === void 0) { firstName = ""; }
        if (lastName === void 0) { lastName = ""; }
        if (email === void 0) { email = ""; }
        if (phoneNumber === void 0) { phoneNumber = ""; }
        if (postCode === void 0) { postCode = ""; }
        if (securityLevel === void 0) { securityLevel = 0; }
        if (balance === void 0) { balance = 0; }
        this._firstName = firstName;
        this._lastName = lastName;
        this._email = email;
        this._phoneNumber = phoneNumber;
        this._postCode = postCode;
        this._securityLevel = securityLevel;
        this._balance = balance;
    }
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "firstName", {
        get: function () {
            return this._firstName;
        },
        set: function (value) {
            this._firstName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "lastName", {
        get: function () {
            return this._lastName;
        },
        set: function (value) {
            this._lastName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "phoneNumber", {
        get: function () {
            return this._phoneNumber;
        },
        set: function (value) {
            this._phoneNumber = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "postCode", {
        get: function () {
            return this._postCode;
        },
        set: function (value) {
            this._postCode = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "securityLevel", {
        get: function () {
            return this._securityLevel;
        },
        set: function (value) {
            this._securityLevel = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "balance", {
        get: function () {
            return this._balance;
        },
        set: function (value) {
            this._balance = value;
        },
        enumerable: true,
        configurable: true
    });
    User.prototype.fromJson = function (json) {
        Object.assign(this, json);
        return this;
    };
    User.prototype.toJson = function () {
        return {
            "id": this._id,
            "firstName": this._firstName,
            "lastName": this._lastName,
            "email": this._email,
            "phoneNumber": this._phoneNumber,
            "postalCode": this._postCode,
            "securityLevel": this._securityLevel,
            "balance": this._balance
        };
    };
    return User;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n<app-list-user [users]=\"users\"></app-list-user>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _User_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./User/user */ "./src/app/User/user.ts");
/* harmony import */ var _User_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./User/user.service */ "./src/app/User/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(userServ) {
        this.userServ = userServ;
        this.title = 'Ticketless';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.getUsers();
    };
    AppComponent.prototype.getUsers = function () {
        var _this = this;
        this._subQueryUser = this.userServ
            .query()
            .subscribe(function (users) {
            return _this._users = users.map(function (user) { return new _User_user__WEBPACK_IMPORTED_MODULE_1__["User"]().fromJson(user); });
        });
    };
    Object.defineProperty(AppComponent.prototype, "users", {
        get: function () {
            return this._users;
        },
        set: function (value) {
            this._users = value;
        },
        enumerable: true,
        configurable: true
    });
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_User_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _camera_webcam_reader_webcam_reader_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./camera/webcam-reader/webcam-reader.component */ "./src/app/camera/webcam-reader/webcam-reader.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_index_index_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/index/index.component */ "./src/app/pages/index/index.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _User_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./User/list-user/list-user.component */ "./src/app/User/list-user/list-user.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    { path: 'camera', component: _camera_webcam_reader_webcam_reader_component__WEBPACK_IMPORTED_MODULE_3__["WebcamReaderComponent"] },
    { path: '', component: _pages_index_index_component__WEBPACK_IMPORTED_MODULE_5__["IndexComponent"], pathMatch: 'full' }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _camera_webcam_reader_webcam_reader_component__WEBPACK_IMPORTED_MODULE_3__["WebcamReaderComponent"],
                _pages_index_index_component__WEBPACK_IMPORTED_MODULE_5__["IndexComponent"],
                _User_list_user_list_user_component__WEBPACK_IMPORTED_MODULE_8__["ListUserComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/camera/webcam-reader/webcam-reader.component.css":
/*!******************************************************************!*\
  !*** ./src/app/camera/webcam-reader/webcam-reader.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\r\n  background-color: #F0F0F0;\r\n}\r\n#app {\r\n  text-align: center;\r\n  color: #2c3e50;\r\n  margin-top: 60px;\r\n}\r\n#video {\r\n  background-color: #000000;\r\n}\r\n#canvas {\r\n  display: none;\r\n}\r\nli {\r\n  display: inline;\r\n  padding: 5px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/camera/webcam-reader/webcam-reader.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/camera/webcam-reader/webcam-reader.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"app\">\r\n  <div><video #video id=\"video\" width=\"640\" height=\"480\" autoplay></video></div>\r\n  <div><button id=\"snap\" (click)=\"capture()\">Snap Photo</button></div>\r\n  <canvas #canvas id=\"canvas\" width=\"640\" height=\"480\"></canvas>\r\n  <ul>\r\n    <li *ngFor=\"let c of captures\">\r\n      <img src=\"{{ c }}\" height=\"50\" />\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/camera/webcam-reader/webcam-reader.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/camera/webcam-reader/webcam-reader.component.ts ***!
  \*****************************************************************/
/*! exports provided: WebcamReaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebcamReaderComponent", function() { return WebcamReaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WebcamReaderComponent = /** @class */ (function () {
    function WebcamReaderComponent() {
        this.captures = [];
    }
    WebcamReaderComponent.prototype.ngOnInit = function () { };
    WebcamReaderComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                _this.video.nativeElement.srcObject = stream;
                _this.video.nativeElement.play();
            });
        }
    };
    WebcamReaderComponent.prototype.capture = function () {
        var context = this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
        this.captures.push(this.canvas.nativeElement.toDataURL('image/png'));
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('video'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WebcamReaderComponent.prototype, "video", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('canvas'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], WebcamReaderComponent.prototype, "canvas", void 0);
    WebcamReaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-webcam-reader',
            template: __webpack_require__(/*! ./webcam-reader.component.html */ "./src/app/camera/webcam-reader/webcam-reader.component.html"),
            styles: [__webpack_require__(/*! ./webcam-reader.component.css */ "./src/app/camera/webcam-reader/webcam-reader.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WebcamReaderComponent);
    return WebcamReaderComponent;
}());



/***/ }),

/***/ "./src/app/pages/index/index.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/index/index.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\r\n  box-sizing: border-box;\r\n\r\n}\r\n@-ms-viewport {\r\n  max-zoom: 200%;\r\n  min-zoom: 50%;\r\n}\r\n@-o-viewport {\r\n  max-zoom: 200%;\r\n  min-zoom: 50%;\r\n}\r\n@viewport {\r\n  max-zoom: 200%;\r\n  min-zoom: 50%;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Black.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Black.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Display_Black.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Display_Black.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Display_Black.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Display_Black.svg#svgFontName') format('svg');\r\n  font-weight: 900;\r\n  font-style: normal;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Bold_Italic.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Bold_Italic.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Display_Bold_Italic.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Display_Bold_Italic.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Display_Bold_Italic.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Display_Bold_Italic.svg#svgFontName') format('svg');\r\n  font-weight: bold;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Thin.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Display_Thin.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Display_Thin.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Display_Thin.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Display_Thin.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Display_Thin.svg#svgFontName') format('svg');\r\n  font-weight: 100;\r\n  font-style: normal;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Bold.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Bold.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Text_Bold.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Text_Bold.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Text_Bold.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Text_Bold.svg#svgFontName') format('svg');\r\n  font-weight: bold;\r\n  font-style: normal;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Italic.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Italic.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Text_Italic.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Text_Italic.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Text_Italic.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Text_Italic.svg#svgFontName') format('svg');\r\n  font-weight: normal;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: 'Faune';\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Regular.eot');\r\n  src:    url('/Scripts/angular/assets/fonts/Faune_Fonts/eot/Faune-Text_Regular.eot?#iefix') format('embedded-opentype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff/Faune-Text_Regular.woff') format('woff'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/woff2/Faune-Text_Regular.woff2') format('woff2'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/ttf/Faune-Text_Regular.ttf') format('truetype'),\r\n  url('/Scripts/angular/assets/fonts/Faune_Fonts/svg/Faune-Text_Regular.svg#svgFontName') format('svg');\r\n  font-weight: normal;\r\n  font-style: normal;\r\n}\r\n/*\r\n * -- BASE STYLES --\r\n * Most of these are inherited from Base, but I want to change a few.\r\n */\r\nbody {\r\n  line-height: 1.7em;\r\n  color: #7f8c8d;\r\n  font-size: 13px;\r\n}\r\nh1,\r\nh2,\r\nh3,\r\nh4,\r\nh5,\r\nh6,\r\nlabel {\r\n  color: #34495e;\r\n}\r\n.pure-img-responsive {\r\n  max-width: 100%;\r\n  height: auto;\r\n}\r\n/*\r\n * -- LAYOUT STYLES --\r\n * These are some useful classes which I will need\r\n */\r\n.l-box {\r\n  padding: 1em;\r\n}\r\n.l-box-lrg {\r\n  padding: 2em;\r\n  border-bottom: 1px solid rgba(0,0,0,0.1);\r\n}\r\n.is-center {\r\n  text-align: center;\r\n}\r\n/*\r\n * -- PURE FORM STYLES --\r\n * Style the form inputs and labels\r\n */\r\n.pure-form label {\r\n  margin: 1em 0 0;\r\n  font-weight: bold;\r\n  font-size: 100%;\r\n}\r\n.pure-form input[type] {\r\n  border: 2px solid #ddd;\r\n  box-shadow: none;\r\n  font-size: 100%;\r\n  width: 100%;\r\n  margin-bottom: 1em;\r\n}\r\n/*\r\n * -- PURE BUTTON STYLES --\r\n * I want my pure-button elements to look a little different\r\n */\r\n.pure-button {\r\n  background-color: #1f8dd6;\r\n  color: white;\r\n  padding: 0.5em 2em;\r\n  border-radius: 5px;\r\n}\r\na.pure-button-primary {\r\n  background: white;\r\n  color: #1f8dd6;\r\n  border-radius: 5px;\r\n  font-size: 120%;\r\n}\r\n/*\r\n * -- MENU STYLES --\r\n * I want to customize how my .pure-menu looks at the top of the page\r\n */\r\n.home-menu {\r\n  padding: 0.5em;\r\n  text-align: center;\r\n  box-shadow: 0 1px 1px rgba(0,0,0, 0.10);\r\n}\r\n.home-menu {\r\n  background: #2d3e50;\r\n}\r\n.pure-menu.pure-menu-fixed {\r\n  /* Fixed menus normally have a border at the bottom. */\r\n  border-bottom: none;\r\n  /* I need a higher z-index here because of the scroll-over effect. */\r\n  z-index: 4;\r\n}\r\n.home-menu .pure-menu-heading {\r\n  font-family: Faune, sans-serif;\r\n  font-style: italic;\r\n  color: white;\r\n  font-weight: 400;\r\n  font-size: 200%;\r\n}\r\n.home-menu .pure-menu-selected a {\r\n  color: white;\r\n  font-family: Faune, sans-serif;\r\n  font-weight: 400;\r\n  font-size: 200%;\r\n}\r\n.home-menu a {\r\n  color: #6FBEF3;\r\n}\r\n.home-menu li a:hover,\r\n.home-menu li a:focus {\r\n  background: none;\r\n  border: none;\r\n  color: #AECFE5;\r\n}\r\n/*\r\n * -- SPLASH STYLES --\r\n * This is the blue top section that appears on the page.\r\n */\r\n.splash-container {\r\n  background: #1f8dd6 url(\"/Scripts/angular/assets/img/common/big.jpeg\") no-repeat;\r\n  z-index: 1;\r\n  overflow: hidden;\r\n  /* The following styles are required for the \"scroll-over\" effect */\r\n  width: 100%;\r\n  height: 88%;\r\n  top: 0;\r\n  left: 0;\r\n  position: fixed !important;\r\n}\r\n@media (min-width: 250px) {\r\n  .splash-container {\r\n    background-image: url(\"/Scripts/angular/assets/img/common/small.jpeg\");\r\n  }\r\n}\r\n@media (min-width: 400px) {\r\n  .splash-container {\r\n    background-image: url(\"/Scripts/angular/assets/img/common/medium.jpeg\");\r\n  }\r\n}\r\n@media (min-width: 800px) {\r\n  .splash-container {\r\n    background-image: url(\"/Scripts/angular/assets/img/common/big.jpeg\");\r\n  }\r\n}\r\n@media (min-width: 2000px) {\r\n  .splash-container {\r\n    background-image: url(\"/Scripts/angular/assets/img/common/immense.jpg\");\r\n  }\r\n}\r\n.splash {\r\n  /* absolute center .splash within .splash-container */\r\n  width: 80%;\r\n  height: 50%;\r\n  margin: auto;\r\n  position: absolute;\r\n  top: 100px; left: 0; bottom: 0; right: 0;\r\n  text-align: center;\r\n  text-transform: uppercase;\r\n}\r\n/* This is the main heading that appears on the blue section */\r\n.splash-head {\r\n  font-size: 20px;\r\n  font-weight: bold;\r\n  color: white;\r\n  border: 3px solid white;\r\n  padding: 1em 1.6em;\r\n  font-weight: 100;\r\n  border-radius: 5px;\r\n  line-height: 1em;\r\n  background-color: rgba(255, 255, 255, .4);\r\n}\r\n/* This is the subheading that appears on the blue section */\r\n.splash-subhead {\r\n  color: white;\r\n  background-color: rgba(86,95,102,.4) ;\r\n  letter-spacing: 0.05em;\r\n  opacity: 0.8;\r\n}\r\n/*\r\n * -- CONTENT STYLES --\r\n * This represents the content area (everything below the blue section)\r\n */\r\n.content-wrapper {\r\n  /* These styles are required for the \"scroll-over\" effect */\r\n  position: absolute;\r\n  top: 87%;\r\n  width: 100%;\r\n  min-height: 12%;\r\n  z-index: 2;\r\n  background: white;\r\n\r\n}\r\n/* We want to give the content area some more padding */\r\n.content {\r\n  padding: 1em 1em 3em;\r\n}\r\n/* This is the class used for the main content headers (<h2>) */\r\n.content-head {\r\n  font-weight: 400;\r\n  text-transform: uppercase;\r\n  letter-spacing: 0.1em;\r\n  margin: 2em 0 1em;\r\n}\r\n/* This is a modifier class used when the content-head is inside a ribbon */\r\n.content-head-ribbon {\r\n  color: white;\r\n}\r\n/* This is the class used for the content sub-headers (<h3>) */\r\n.content-subhead {\r\n  color: #1f8dd6;\r\n}\r\n.content-subhead i {\r\n  margin-right: 7px;\r\n}\r\n/* This is the class used for the dark-background areas. */\r\n.ribbon {\r\n  background: #2d3e50;\r\n  color: #aaa;\r\n}\r\n/* This is the class used for the footer */\r\n.footer {\r\n  background: #111;\r\n  position: fixed;\r\n  bottom: 0;\r\n  width: 100%;\r\n}\r\n/*\r\n * -- TABLET (AND UP) MEDIA QUERIES --\r\n * On tablets and other medium-sized devices, we want to customize some\r\n * of the mobile styles.\r\n */\r\n@media (min-width: 48em) {\r\n\r\n  /* We increase the body font size */\r\n  body {\r\n    font-size: 16px;\r\n  }\r\n\r\n  /* We can align the menu header to the left, but float the\r\n  menu items to the right. */\r\n  .home-menu {\r\n    text-align: left;\r\n  }\r\n  .home-menu ul {\r\n    float: right;\r\n  }\r\n\r\n  /* We increase the height of the splash-container */\r\n  /*    .splash-container {\r\n          height: 500px;\r\n      }*/\r\n\r\n  /* We decrease the width of the .splash, since we have more width\r\n  to work with */\r\n  .splash {\r\n    width: 50%;\r\n    height: 50%;\r\n  }\r\n\r\n  .splash-head {\r\n    font-size: 250%;\r\n  }\r\n\r\n\r\n  /* We remove the border-separator assigned to .l-box-lrg */\r\n  .l-box-lrg {\r\n    border: none;\r\n  }\r\n\r\n}\r\n/*\r\n * -- DESKTOP (AND UP) MEDIA QUERIES --\r\n * On desktops and other large devices, we want to over-ride some\r\n * of the mobile and tablet styles.\r\n */\r\n@media (min-width: 78em) {\r\n  /* We increase the header font size even more */\r\n  .splash-head {\r\n    font-size: 300%;\r\n  }\r\n}\r\n"

/***/ }),

/***/ "./src/app/pages/index/index.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/index/index.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\r\n  <div class=\"home-menu pure-menu pure-menu-horizontal pure-menu-fixed\">\r\n    <a class=\"pure-menu-heading\" href=\"\">TicketLess</a>\r\n\r\n    <ul class=\"pure-menu-list\">\r\n      <li class=\"pure-menu-item pure-menu-selected\"><a href=\"#\" class=\"pure-menu-link\">Home</a></li>\r\n      <li class=\"pure-menu-item\"><a href=\"#\" class=\"pure-menu-link\">Tour</a></li>\r\n      <li class=\"pure-menu-item\"><a href=\"#\" class=\"pure-menu-link\">Sign Up</a></li>\r\n    </ul>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"splash-container\">\r\n  <div class=\"splash\">\r\n    <h1 class=\"splash-head\">NO MORE TICKETS</h1>\r\n    <p class=\"splash-subhead\">\r\n      Lorem ipsum dolor sit amet, consectetur adipisicing elit.\r\n    </p>\r\n    <p>\r\n      <a routerLink=\"camera\" class=\"pure-button pure-button-primary\" >Get Started</a>\r\n    </p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"content-wrapper\">\r\n  <div class=\"content\">\r\n    <h2 class=\"content-head is-center\">Excepteur sint occaecat cupidatat.</h2>\r\n    <div class=\"pure-g\">\r\n      <div class=\"l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4\">\r\n        <h3 class=\"content-subhead\">\r\n          <i class=\"fa fa-rocket\"></i>\r\n          Get Started Quickly\r\n        </h3>\r\n        <p>\r\n          Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.\r\n        </p>\r\n      </div>\r\n      <div class=\"l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4\">\r\n        <h3 class=\"content-subhead\">\r\n          <i class=\"fa fa-mobile\"></i>\r\n          Responsive Layouts\r\n        </h3>\r\n        <p>\r\n          Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.\r\n        </p>\r\n      </div>\r\n      <div class=\"l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4\">\r\n        <h3 class=\"content-subhead\">\r\n          <i class=\"fa fa-th-large\"></i>\r\n          Modular\r\n        </h3>\r\n        <p>\r\n          Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.\r\n        </p>\r\n      </div>\r\n      <div class=\"l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4\">\r\n        <h3 class=\"content-subhead\">\r\n          <i class=\"fa fa-check-square-o\"></i>\r\n          Plays Nice\r\n        </h3>\r\n        <p>\r\n          Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"ribbon l-box-lrg pure-g\">\r\n    <div class=\"l-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5\">\r\n      <!-- <img width=\"300\" alt=\"File Icons\" class=\"pure-img-responsive\" src=\"assets/img/common/file-icons.png\"> -->\r\n    </div>\r\n    <div class=\"pure-u-1 pure-u-md-1-2 pure-u-lg-3-5\">\r\n      <h2 class=\"content-head content-head-ribbon\">Say Cheese <3</h2>\r\n      <!-- <app-webcam-reader></app-webcam-reader> -->\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"content\">\r\n    <h2 class=\"content-head is-center\">Dolore magna aliqua. Uis aute irure.</h2>\r\n\r\n    <div class=\"pure-g\">\r\n      <div class=\"l-box-lrg pure-u-1 pure-u-md-2-5\">\r\n        <form class=\"pure-form pure-form-stacked\">\r\n          <fieldset>\r\n\r\n            <label for=\"name\">Your Name</label>\r\n            <input id=\"name\" type=\"text\" placeholder=\"Your Name\">\r\n\r\n\r\n            <label for=\"email\">Your Email</label>\r\n            <input id=\"email\" type=\"email\" placeholder=\"Your Email\">\r\n\r\n            <label for=\"password\">Your Password</label>\r\n            <input id=\"password\" type=\"password\" placeholder=\"Your Password\">\r\n\r\n            <button type=\"submit\" class=\"pure-button\">Sign Up</button>\r\n          </fieldset>\r\n        </form>\r\n      </div>\r\n\r\n      <div class=\"l-box-lrg pure-u-1 pure-u-md-3-5\">\r\n        <h4>Contact Us</h4>\r\n        <p>\r\n          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n          consequat.\r\n        </p>\r\n\r\n        <h4>More Information</h4>\r\n        <p>\r\n          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n          tempor incididunt ut labore et dolore magna aliqua.\r\n        </p>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div class=\"footer l-box is-center\">\r\n    View the source of this layout to learn more. Made with love by the YUI Team.\r\n  </div>\r\n\r\n</div>\r\n\r\n<script>\r\n  (function (root) {\r\n    /* -- Data -- */\r\n    root.YUI_config = {\"version\":\"3.18.1\",\"base\":\"http:\\u002F\\u002Fyui.yahooapis.com\\u002F3.18.1\\u002F\",\"comboBase\":\"https:\\u002F\\u002Fyui-s.yahooapis.com\\u002Fcombo?\",\"comboSep\":\"&\",\"root\":\"3.18.1\\u002F\",\"filter\":\"min\",\"logLevel\":\"error\",\"combine\":true,\"patches\":[],\"maxURLLength\":2048,\"groups\":{\"vendor\":{\"combine\":true,\"comboBase\":\"\\u002Fcombo\\u002F1.18.13?\",\"base\":\"\\u002F\",\"root\":\"\\u002F\",\"modules\":{\"css-mediaquery\":{\"path\":\"vendor\\u002Fcss-mediaquery.js\"},\"handlebars-runtime\":{\"path\":\"vendor\\u002Fhandlebars.runtime.js\"}}},\"app\":{\"combine\":true,\"comboBase\":\"\\u002Fcombo\\u002F1.18.13?\",\"base\":\"\\u002Fjs\\u002F\",\"root\":\"\\u002Fjs\\u002F\"}}};\r\n    root.app || (root.app = {});\r\n    root.app.yui = {\"use\":function () { return this._bootstrap('use', [].slice.call(arguments)); },\"require\":function () { this._bootstrap('require', [].slice.call(arguments)); },\"ready\":function (callback) { this.use(function () { callback(); }); },\"_bootstrap\":function bootstrap(method, args) { var self = this, d = document, head = d.getElementsByTagName('head')[0], ie = /MSIE/.test(navigator.userAgent), callback = [], config = typeof YUI_config != \"undefined\" ? YUI_config : {}; function flush() { var l = callback.length, i; if (!self.YUI && typeof YUI == \"undefined\") { throw new Error(\"YUI was not injected correctly!\"); } self.YUI = self.YUI || YUI; for (i = 0; i < l; i++) { callback.shift()(); } } function decrementRequestPending() { self._pending--; if (self._pending <= 0) { setTimeout(flush, 0); } else { load(); } } function createScriptNode(src) { var node = d.createElement('script'); if (node.async) { node.async = false; } if (ie) { node.onreadystatechange = function () { if (/loaded|complete/.test(this.readyState)) { this.onreadystatechange = null; decrementRequestPending(); } }; } else { node.onload = node.onerror = decrementRequestPending; } node.setAttribute('src', src); return node; } function load() { if (!config.seed) { throw new Error('YUI_config.seed array is required.'); } var seed = config.seed, l = seed.length, i, node; if (!self._injected) { self._injected = true; self._pending = seed.length; } for (i = 0; i < l; i++) { node = createScriptNode(seed.shift()); head.appendChild(node); if (node.async !== false) { break; } } } callback.push(function () { var i; if (!self._Y) { self.YUI.Env.core.push.apply(self.YUI.Env.core, config.extendedCore || []); self._Y = self.YUI(); self.use = self._Y.use; if (config.patches && config.patches.length) { for (i = 0; i < config.patches.length; i += 1) { config.patches[i](self._Y, self._Y.Env._loader); } } } self._Y[method].apply(self._Y, args); }); self.YUI = self.YUI || (typeof YUI != \"undefined\" ? YUI : null); if (!self.YUI && !self._injected) { load(); } else if (self._pending <= 0) { setTimeout(flush, 0); } return this; }};\r\n    root.YUI_config || (root.YUI_config = {});\r\n    root.YUI_config.seed = [\"https:\\u002F\\u002Fyui-s.yahooapis.com\\u002Fcombo?3.18.1\\u002Fyui\\u002Fyui-min.js\"];\r\n    root.YUI_config.groups || (root.YUI_config.groups = {});\r\n    root.YUI_config.groups.app || (root.YUI_config.groups.app = {});\r\n    root.YUI_config.groups.app.modules = {\"start\\u002Fapp\":{\"path\":\"start\\u002Fapp.js\",\"requires\":[\"handlebars-runtime\",\"yui\",\"base-build\",\"router\",\"pjax-base\",\"view\",\"start\\u002Fmodels\\u002Fgrid\",\"start\\u002Fviews\\u002Finput\",\"start\\u002Fviews\\u002Foutput\",\"start\\u002Fviews\\u002Fdownload\"]},\"start\\u002Fmodels\\u002Fgrid\":{\"path\":\"start\\u002Fmodels\\u002Fgrid.js\",\"requires\":[\"yui\",\"querystring\",\"base-build\",\"model\",\"model-sync-rest\",\"start\\u002Fmodels\\u002Fmq\"]},\"start\\u002Fmodels\\u002Fmq\":{\"path\":\"start\\u002Fmodels\\u002Fmq.js\",\"requires\":[\"css-mediaquery\",\"attribute\",\"base-build\",\"model\",\"model-list\"]},\"start\\u002Fviews\\u002Fdownload\":{\"path\":\"start\\u002Fviews\\u002Fdownload.js\",\"requires\":[\"yui\",\"base-build\",\"querystring\",\"view\"]},\"start\\u002Fviews\\u002Finput\":{\"path\":\"start\\u002Fviews\\u002Finput.js\",\"requires\":[\"base-build\",\"start\\u002Fmodels\\u002Fmq\",\"start\\u002Fviews\\u002Ftab\"]},\"start\\u002Fviews\\u002Foutput\":{\"path\":\"start\\u002Fviews\\u002Foutput.js\",\"requires\":[\"base-build\",\"escape\",\"start\\u002Fviews\\u002Ftab\"]},\"start\\u002Fviews\\u002Ftab\":{\"path\":\"start\\u002Fviews\\u002Ftab.js\",\"requires\":[\"yui\",\"base-build\",\"view\"]}};\r\n  }(this));\r\n</script>\r\n<script>\r\n  app.yui.use('node-base', 'node-event-delegate', function (Y) {\r\n    // This just makes sure that the href=\"#\" attached to the <a> elements\r\n    // don't scroll you back up the page.\r\n    Y.one('body').delegate('click', function (e) {\r\n      e.preventDefault();\r\n    }, 'a[href=\"#\"]');\r\n  });\r\n</script>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/index/index.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/index/index.component.ts ***!
  \************************************************/
/*! exports provided: IndexComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexComponent", function() { return IndexComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IndexComponent = /** @class */ (function () {
    function IndexComponent() {
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-index',
            template: __webpack_require__(/*! ./index.component.html */ "./src/app/pages/index/index.component.html"),
            styles: [__webpack_require__(/*! ./index.component.css */ "./src/app/pages/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], IndexComponent);
    return IndexComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\media\Documents\Cours\3ème\Ti\projets\ticketless\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map