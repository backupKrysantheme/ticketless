import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';
import {Subscription} from 'rxjs';
import {Merch} from '../../merch/merch';
import {MerchService} from '../../merch/merch.service';
import {Promo} from '../../promo/promo';
import {PromoService} from '../../promo/promo.service';
import {UserSessionService} from '../../user/user-session.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  private _userToModify: User;
  private _users: User[];
  private _userQuerySub: Subscription;
  private _userDeleteSub: Subscription;
  private _userAddSub: Subscription;
  private _userUpdateSub: Subscription;

  private _merchToModify: Merch;
  private _merchs: Merch[];
  private _merchQuerySub: Subscription;
  private _merchDeleteSub: Subscription;
  private _merchAddSub: Subscription;
  private _merchUpdateSub: Subscription;

  private _promoToModify: Promo;
  private _promos: Promo[];
  private _promoQuerySub: Subscription;
  private _promoDeleteSub: Subscription;
  private _promoUpdateSub: Subscription;
  private _promoAddSub: Subscription;
  private _editingUser: boolean;
  private _editingMerch: boolean;
  private _editingPromo: boolean;

  constructor(public userService: UserService, public merchService: MerchService, public promoService: PromoService, public userSession: UserSessionService,
              public router: Router, public toastr: ToastrService) {
  }

  ngOnInit() {
    if (this.userSession.user == null || this.userSession.user.securityLevel == 0) {
      this.router.navigate(['']);
    }

    this._userQuerySub = this.userService.query().subscribe(
      users => {
        this._users = users.map(user => new User().fromJson(user));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      });

    this._merchQuerySub = this.merchService.query().subscribe(
      merchs => {
        this._merchs = merchs.map(merch => new Merch().fromJson(merch));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      });

    this._promoQuerySub = this.promoService.query().subscribe(
      promos => {
        this._promos = promos.map(promo => new Promo().fromJson(promo));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      });
  }

  ngOnDestroy() {
    if (this._userQuerySub != null)
      this._userQuerySub.unsubscribe();
    if (this._userDeleteSub != null)
      this._userDeleteSub.unsubscribe();
    if (this._userAddSub != null)
      this._userAddSub.unsubscribe();
    if (this._userUpdateSub != null)
      this._userUpdateSub.unsubscribe();

    if (this._merchQuerySub != null)
      this._merchQuerySub.unsubscribe();
    if (this._merchDeleteSub != null)
      this._merchDeleteSub.unsubscribe();
    if (this._merchAddSub != null)
      this._merchAddSub.unsubscribe();
    if (this._merchUpdateSub != null)
      this._merchUpdateSub.unsubscribe();

    if (this._promoQuerySub != null)
      this._promoQuerySub.unsubscribe();
    if (this._promoDeleteSub != null)
      this._promoDeleteSub.unsubscribe();
    if (this._promoAddSub != null)
      this._promoAddSub.unsubscribe();
    if (this._promoUpdateSub != null)
      this._promoUpdateSub.unsubscribe();
  }

  get users(): User[] {
    return this._users;
  }

  get merchs(): Merch[] {
    return this._merchs;
  }

  public onPromoAdded(promo: Promo) {
    if (promo != null) {
      //update
      if (promo.id > 0) {
        this._promoUpdateSub = this.promoService.update(promo).subscribe(
          updatePromo => {
            let i: number = this.promos.findIndex(promo => promo.id == updatePromo.id);
            this.promos[i] = updatePromo;
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      } else {
        this._promoAddSub = this.promoService.post(promo).subscribe(
          newPromo => {
            this.promos.push(new Promo().fromJson(newPromo));
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
    }
    this._editingPromo = false;
    this._promoToModify = null;
  }

  public onUserAdded(user: User) {
    if (user != null) {
      //update
      if (user.id > 0) {
        this._userUpdateSub = this.userService.update(user).subscribe(
          updatedUser => {
            let i: number = this.users.findIndex(user => user.id == updatedUser.id);
            this.users[i] = new User().fromJson(updatedUser);
            if(this.userSession.user.id == this.users[i].id)
            {
              this.users[i].cart = this.userSession.user.cart;
              this.userSession.user = this.users[i];
              this.userSession.saveSession();
            }
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
      //Add
      else {
        this._userAddSub = this.userService.post(user).subscribe(
          newUser => {
            this.users.push(new User().fromJson(newUser));
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
    }
    this._editingUser = false;
    this._userToModify = null;
  }

  public onUserDeleted(user: User) {
    this._userDeleteSub = this.userService.delete(user).subscribe(
      () => {
        this._users.splice(this._users.map(t => t.id).indexOf(user.id));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      }
    );
  }

  public onMerchAdded(merch: Merch) {
    if (merch != null) {
      //Update
      if (merch.id > 0) {
        this._merchUpdateSub = this.merchService.update(merch).subscribe(
          updatedMerch => {
            let i: number = this.merchs.findIndex(merch => merch.id == updatedMerch.id);
            this.merchs[i] = updatedMerch;
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
      //Add
      else {
        this._merchAddSub = this.merchService.post(merch).subscribe(
          newMerch => {
            this.merchs.push(new Merch().fromJson(newMerch));
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
    }
    this._editingMerch = false;
    this._merchToModify = null;
  }

  public onMerchDeleted(merch: Merch) {
    this._merchDeleteSub = this.merchService.delete(merch).subscribe(
      () => {
        this._merchs.splice(this._merchs.map(t => t.id).indexOf(merch.id));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      }
    );
  }

  public onPromoDeleted(promo: Promo) {
    this._promoDeleteSub = this.promoService.delete(promo).subscribe(
      () => {
        this._merchs.splice(this._merchs.map(t => t.id).indexOf(promo.id));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      }
    );
  }

  public onUserModify(user: User) {
    this._userToModify = user;
    this._editingUser = true;
  }

  public onPromoModify(promo: Promo) {
    this._promoToModify = promo;
    this._editingPromo = true;
  }

  public onMerchModify(merch: Merch) {
    this._merchToModify = merch;
    this._editingMerch = true;
  }

  get promos(): Promo[] {
    return this._promos;
  }

  private toggleEditingUser() {
    this._editingUser = !this._editingUser;
  }

  private toggleEditingMerch() {
    this._editingMerch = !this._editingMerch;
  }

  private toggleEditingPromo() {
    this._editingPromo = !this._editingPromo;
  }

  get editingMerch(): boolean {
    return this._editingMerch;
  }

  get editingUser(): boolean {
    return this._editingUser;
  }

  get userToModify(): User {
    return this._userToModify;
  }

  get merchToModify(): Merch {
    return this._merchToModify;
  }

  get editingPromo(): boolean {
    return this._editingPromo;
  }

  get promoToModify(): Promo {
    return this._promoToModify;
  }
}
