import {Component, Input, OnInit} from '@angular/core';



@Component({
  selector: 'app-confirmation-inscription',
  templateUrl: './confirmation-inscription.component.html',
  styleUrls: ['./confirmation-inscription.component.css']
})
export class ConfirmationInscriptionComponent implements OnInit {


  private  _elementType : 'dataurl';
  private _qrDataValue : string;

  constructor()
  {

  }

  ngOnInit()
  {

  }

  public get elementType() : string
  {
    return this._elementType;
  }

  public get qrDataValue() : string {
    return this._qrDataValue;
  }

  @Input()
  public set qrDataValue(value : string ) {
    this._qrDataValue = value;
  }

}
