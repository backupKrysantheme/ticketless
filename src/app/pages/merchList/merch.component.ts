import {Component, OnInit, OnDestroy} from '@angular/core';
import {Merch} from '../../merch/merch';
import {MerchService} from '../../merch/merch.service';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-merch',
  templateUrl: './merch.component.html',
  styleUrls: ['./merch.component.css']
})
export class MerchComponent implements OnInit, OnDestroy {

  private _merchs: Merch[];
  private _merchQuerySub: Subscription;

  constructor(public merchService: MerchService, public toastr: ToastrService) {
  }

  ngOnInit() {
    this._merchQuerySub = this.merchService.query().subscribe(
      merchs => {
        this._merchs = merchs.map(merch => new Merch().fromJson(merch));
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      }
    );
  }

  get merchs(): Merch[] {
    return this._merchs;
  }

  ngOnDestroy() {
    if (this._merchQuerySub != null)
      this._merchQuerySub.unsubscribe();
  }

}
