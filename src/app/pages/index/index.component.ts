import {Component, OnInit} from '@angular/core';
import {UserService} from '../../user/user.service';
import {UserSessionService} from '../../user/user-session.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private _isUserConnected: boolean;

  constructor(public sessionService: UserSessionService) {
  }

  ngOnInit() {
    if (this.sessionService.user != null) {
      this._isUserConnected = true;
    } else {
      this._isUserConnected = false;
    }
  }

  get isUserConnected(): boolean {
    return this._isUserConnected;
  }

}
