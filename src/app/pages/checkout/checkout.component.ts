import {Component, OnDestroy, OnInit} from '@angular/core';
import {Merch} from '../../merch/merch';
import {UserSessionService} from '../../user/user-session.service';
import {Promo} from '../../promo/promo';
import {PromoService} from '../../promo/promo.service';
import {Subscription} from 'rxjs';
import {TransactionService} from '../../user/transaction.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy {

  private _cart: Merch[] = [];
  private _promos: Promo[];
  private _promoSub: Subscription;
  private _postSub: Subscription;
  private _amounts: number[] = [];
  private _total: number = 0;
  private _totalRed: number;
  private _totalWithRed: number;

  constructor(public userSession: UserSessionService, public promoService: PromoService, public transactionService: TransactionService,
              public toastr: ToastrService, public router: Router) {
  }

  ngOnInit() {
    this._promoSub = this.promoService.query().subscribe(promos => {
        this._promos = promos.map(promo => new Promo().fromJson(promo));

        this.userSession.user.cart
          .map(merch => {
            let index: number = this._cart.findIndex(m => m.id == merch.id);
            if (index > -1) {
              this._amounts[index] += 1;
            } else {
              this._cart.push(merch);
              this._amounts.push(1);
            }
          });

        this.calculTotal();
      }
    );
  }

  get cart(): Merch[] {
    return this._cart;
  }

  public changeAmounts(amounts: number[]) {
    this._amounts = amounts;
    this.calculTotal();
  }

  public deleteCartItem(merchToDelete:Merch) {
    this.userSession.user.removeMerchFromCart(merchToDelete);
    let i:number = this._cart.findIndex(merch => merch.id == merchToDelete.id);
    this._cart.splice(i,1);
    this._amounts.splice(i,1);
  }

  public calculTotal() {
    this._total = 0;
    for (let i = 0; i < this._cart.length; i++) {
      this._total += this._cart[i].price * this._amounts[i];
    }

    this.calculPromos();

    this._totalWithRed = this._total - this._totalRed;
  }

  public calculPromos() {
    this._totalRed = 0;

    for (let i = 0; i < this._cart.length; i++) {
      let index: number = this._promos.findIndex(promo => promo.id == this._cart[i].promoId);
      if (index != -1) {
        this._totalRed += (this._cart[i].price * (this._promos[index].percentage / 100)) * this._amounts[i];
      }
    }
  }

  get amounts(): number[] {
    return this._amounts;
  }

  get total(): number {
    return this._total;
  }

  set total(value: number) {
    this._total = value;
  }

  get totalRed(): number {
    return this._totalRed;
  }

  ngOnDestroy(): void {
    this._promoSub.unsubscribe();
    if (this._postSub != null)
      this._postSub.unsubscribe();
  }

  get totalWithRed(): number {
    return this._totalWithRed;
  }

  public onCheckout() {
    this._postSub = this.transactionService.post(this.userSession.user, this._cart, this._amounts).subscribe(
      successMessage => {
        this.userSession.user.balance -= this._totalWithRed;
        this.userSession.user.clearCart();
        this.userSession.saveSession();

        this._cart = [];
        this._amounts = [];
        this.calculTotal();

        this.toastr.success(successMessage, 'Success', {positionClass: 'toast-bottom-center'});

        this.router.navigate(['']);
      },
      error => {
        this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
      }
    );
  }
}
