import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Merch} from '../../merch/merch';
import {filters, FilterMerchPipe} from '../../merch/filter-merch.pipe'
import {isNumber} from 'util';

@Component({
  selector: 'app-list-merch-client',
  templateUrl: './list-merch-client.component.html',
  styleUrls: ['./list-merch-client.component.css']
})
export class ListMerchClientComponent implements OnInit
{
  //Fill the array with the filters
  private _OPTIONS:filters[] = Object.values(filters).filter(
    (type) => isNumber(<any>type)
  );

  private _merchs: Merch[] = [];
  private _optionSelected = filters.all;

  constructor() {
  }

  ngOnInit() {
  }

  get merchs(): Merch[] {
    return this._merchs;
  }

  @Input()
  set merchs(value: Merch[]) {
    this._merchs = value;
  }

  intToNameOfOption(num: number) {
    return FilterMerchPipe.intToNameOfOption(num);
  }

  get optionSelected(): filters {
    return this._optionSelected;
  }


  set optionSelected(value: filters) {
    this._optionSelected = value;
  }

  get OPTIONS(): filters[] {
    return this._OPTIONS;
  }
}
