import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMerchClientComponent } from './list-merch-client.component';

describe('ListMerchClientComponent', () => {
  let component: ListMerchClientComponent;
  let fixture: ComponentFixture<ListMerchClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMerchClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMerchClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
