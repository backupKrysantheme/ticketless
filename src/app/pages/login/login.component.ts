import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../user/user';
import {LoginService} from '../../user/login.service';
import {Subscription} from 'rxjs';
import {UserSessionService} from '../../user/user-session.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  private _user: User;
  private _subPostUser: Subscription;
  private _loginByQR: boolean = false;
  private _loginForm = new FormGroup({
    mail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    password: new FormControl('', Validators.required)
  });

  constructor(public loginService: LoginService, public userSessionService: UserSessionService, public router: Router,
              public toastr: ToastrService) {
  }

  ngOnInit() {
    this._user = this.userSessionService.user;
  }

  get user(): User {
    return this._user;
  }

  ngOnDestroy(): void {
    if (this._subPostUser) {
      this._subPostUser.unsubscribe();
    }
  }

  get loginByQR(): boolean {
    return this._loginByQR;
  }

  public verifyUser(qr: string) {
    this._subPostUser = this.loginService.post(this.loginForm.controls.mail.value.toString(), this.loginForm.controls.password.value.toString(), qr)
      .subscribe(
        newUser => {
          this._user = new User().fromJson(newUser);

          //Call the session service to save the user in the local storage
          this.userSessionService.user = this._user;

          this._loginByQR = false;

          this.router.navigate(['']);
        },
        error => {
          this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
        });
  }

  public toggleQRScan() {
    this._loginByQR = true;
  }

  get loginForm(): FormGroup {
    return this._loginForm;
  }


}
