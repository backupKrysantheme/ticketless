import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';
import {Subscription} from 'rxjs';
import {UserSessionService} from '../../user/user-session.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  private _subPostUser: Subscription;
  private _registerForm = new FormGroup({
    mail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    password: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)]))
  });

  private _newUser: User;
  private _userCreated: boolean = false;
  private _qrDataValue: string = '';

  constructor(public userService: UserService, public userSessionService: UserSessionService, public toastr: ToastrService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._subPostUser) {
      this._subPostUser.unsubscribe();
    }
  }

  public createNewUser() {
    this._newUser = new User(this._registerForm.controls.firstName.value.toString(),
      this._registerForm.controls.lastName.value.toString(),
      this._registerForm.controls.mail.value.toString(),
      this._registerForm.controls.phoneNumber.value.toString(),
      this._registerForm.controls.postalCode.value.toString(),
      0,
      0,
      this._registerForm.controls.password.value.toString(),
      this.qrDataValue
    );
  }

  public postUser() {
    if (this._registerForm.controls.password.value.toString() === this._registerForm.controls.confirmPassword.value.toString()) {
      this.createNewUser();
      this._subPostUser = this.userService.post(this.newUser).subscribe(newUser => {
          this._newUser = new User().fromJson(newUser);
          this.userSessionService.user = this._newUser;
          this._userCreated = true;
        },
        error => {
          this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
        });
    } else {
      this.toastr.error('The password must match the confirmation password', 'Error', {positionClass: 'toast-bottom-center'});
    }
  }

  getNewUserQrCode() {
    return this.newUser.qrcode;
  }

  get registerForm(): FormGroup {
    return this._registerForm;
  }

  get newUser(): User {
    return this._newUser;
  }

  get userCreated(): boolean {
    return this._userCreated;
  }

  get qrDataValue(): string {
    return this._qrDataValue;
  }
}
