import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../user/user';
import {History} from '../../history/history';
import {UserService} from '../../user/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {HistoryService} from '../../history/history.service';
import {TransactionService} from '../../user/transaction.service';
import {UserSessionService} from '../../user/user-session.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-detail-user-smart',
  templateUrl: './detail-user-smart.component.html',
  styleUrls: ['./detail-user-smart.component.css']
})
export class DetailUserSmartComponent implements OnInit, OnDestroy {

  private _user: User = new User();
  private _subParamsRoute: Subscription;
  private _subUserService: Subscription;
  private _transactionPutSub: Subscription;
  private _subHistory: Subscription;
  private _updateAccount: boolean;
  private _userUpdateSub: Subscription;
  private _histories: History[];
  private _isAdmin: boolean;

  constructor(public userService: UserService, public activatedRoute: ActivatedRoute, public transactionServ: TransactionService
    , public userSession: UserSessionService, public router: Router, public historyService: HistoryService,
              public toastr: ToastrService) {
  }

  ngOnInit() {
    this.fetchUserViaIdUrl();
  }

  private fetchUserViaIdUrl() {
    this._subParamsRoute = this.activatedRoute.params.subscribe(params => {
        this._subUserService = this.userService.get(+params.id).subscribe(
          user => {
            this._user = new User().fromJson(user);

            if (this.userSession.user.securityLevel == 0 && (this.userSession.user == null || this.userSession.user.id != this._user.id)) {
              this.router.navigate(['']);
            } else {
              if (this.userSession.user.securityLevel == 1 && this.userSession.user.id != this._user.id) {
                this._isAdmin = true;
              }
              this._subHistory = this.historyService.get(this._user.id).subscribe(
                histories => {
                  this._histories = histories.map(history => new History().fromJson(history));
                },
                error => {
                  this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
                }
              );
            }
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      },
      error => {
        this.toastr.error(error.error.Message, 'Error', {positionClass: 'toast-bottom-center'});
      });
  }

  ngOnDestroy(): void {
    if (this._subParamsRoute != null)
      this._subParamsRoute.unsubscribe();
    if (this._subUserService != null)
      this._subUserService.unsubscribe();
    if (this._transactionPutSub != null)
      this._transactionPutSub.unsubscribe();
    if (this._userUpdateSub != null)
      this._userUpdateSub.unsubscribe();
    if (this._subHistory != null)
      this._subHistory.unsubscribe();
  }

  get histories(): History[] {
    return this._histories;
  }

  get user(): User {
    return this._user;
  }

  public onChargeAccount(val: number) {
    if (this._user.balance + val > 99999.99) {
      this.toastr.error('Your balance cannot exceed 99999.99€', 'Error', {positionClass: 'toast-bottom-center'});
    } else {
      this._user.balance += val;
      this._transactionPutSub = this.transactionServ.update(this._user).subscribe(
        () => {
          this.userSession.user.balance += val;
          this.userSession.saveSession();
        },
        error => {
          this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
        }
      );
    }
  }

  public onTriggerUpdate(bool: boolean) {
    if (bool) {
      this.toggleUpdateAccount();
    }
  }

  public toggleUpdateAccount() {
    this._updateAccount = !this._updateAccount;
  }

  public onUserUpdated(user: User) {
    if (user != null) {
      if (user.id > 0) {
        this._userUpdateSub = this.userService.update(user).subscribe(
          updatedUser => {
            this._user = new User().fromJson(updatedUser);
            this._user.cart = this.userSession.user.cart;
            this.userSession.user = this._user;
            this.userSession.saveSession();
          },
          error => {
            this.toastr.error(error.error.Message, 'Server error', {positionClass: 'toast-bottom-center'});
          }
        );
      }
    }
    this.toggleUpdateAccount();
  }

  get updateAccount(): boolean {
    return this._updateAccount;
  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }
}
