import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUserSmartComponent } from './detail-user-smart.component';

describe('DetailUserSmartComponent', () => {
  let component: DetailUserSmartComponent;
  let fixture: ComponentFixture<DetailUserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailUserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
