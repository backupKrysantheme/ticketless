import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {MerchService} from '../../merch/merch.service';
import {Merch} from '../../merch/merch';
import {UserSessionService} from '../../user/user-session.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-detail-merch-smart',
  templateUrl: './detail-merch-smart.component.html',
  styleUrls: ['./detail-merch-smart.component.css']
})
export class DetailMerchSmartComponent implements OnInit, OnDestroy {
  private _merch: Merch;
  private _subParamsRoute: Subscription;
  private _subMerchService: Subscription;

  constructor(public merchSevice: MerchService, public activatedRoute: ActivatedRoute, public userSession: UserSessionService, public toastr: ToastrService) {
  }

  ngOnInit() {
    this.fetchUserViaIdUrl();
  }

  private fetchUserViaIdUrl() {
    this._subParamsRoute = this.activatedRoute.params.subscribe(params => {
      this._subMerchService = this.merchSevice.get(+params.id).subscribe(
        merch => {
          this._merch = new Merch().fromJson(merch);
        },
        error => {
          this.toastr.error(error.message.Message, 'MerchNotFound', {positionClass: 'toast-bottom-center'});
        }
      );
    });
  }

  public onAddToCart(merch: Merch) {
    if (this.userSession.user != null) {
      this.userSession.user.addMerch(merch);
      this.userSession.saveSession();
      this.toastr.success('This ' + this.merch.label + ' has been added to your cart.', 'Good catch !', {positionClass: 'toast-bottom-center'});
    }
  }

  ngOnDestroy(): void {
    if(this._subParamsRoute != null)
      this._subParamsRoute.unsubscribe();
    if(this._subMerchService != null)
      this._subMerchService.unsubscribe();
  }

  get merch(): Merch {
    return this._merch;
  }
}
