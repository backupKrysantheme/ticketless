import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailMerchSmartComponent } from './detail-merch-smart.component';

describe('DetailMerchSmartComponent', () => {
  let component: DetailMerchSmartComponent;
  let fixture: ComponentFixture<DetailMerchSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailMerchSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailMerchSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
