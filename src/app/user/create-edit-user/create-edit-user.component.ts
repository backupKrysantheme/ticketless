import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../user';

@Component({
  selector: 'app-create-edit-user',
  templateUrl: './create-edit-user.component.html',
  styleUrls: ['./create-edit-user.component.css']
})
export class CreateEditUserComponent implements OnInit {

  private _user: User;
  private _addUserEvent: EventEmitter<User> = new EventEmitter();
  private _modifying: boolean;
  private _isAdmin: boolean;

  private _userForm = new FormGroup({
    mail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    password: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    security: new FormControl('', Validators.required),
    balance: new FormControl('', Validators.required),
    postalCode: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)]))
  });

  constructor() {
  }

  ngOnInit() {
  }

  public onClickAddUser(cancel: boolean) {
    if (!cancel) {
      if (!this._modifying) {
        this._user = new User(
          this._userForm.controls.firstName.value,
          this._userForm.controls.lastName.value,
          this._userForm.controls.mail.value,
          this._userForm.controls.phoneNumber.value,
          this._userForm.controls.postalCode.value,
          this._userForm.controls.security.value,
          this._userForm.controls.balance.value,
          this._userForm.controls.password.value,
        );
      } else {
        this._user.firstName = this._userForm.controls.firstName.value;
        this._user.lastName = this._userForm.controls.lastName.value;
        this._user.email = this._userForm.controls.mail.value;
        this._user.phoneNumber = this._userForm.controls.phoneNumber.value;
        this._user.postalCode = this._userForm.controls.postalCode.value;
        this._user.securityLevel = this._userForm.controls.security.value;
        this._user.balance = this._userForm.controls.balance.value;
        this._user.passwd = this._userForm.controls.password.value;
      }
      this._addUserEvent.next(this._user);
    } else {
      this._addUserEvent.next(null);
    }
  }

  @Output()
  get onUserAdded(): EventEmitter<User> {
    return this._addUserEvent;
  }

  get userForm(): FormGroup {
    return this._userForm;
  }

  @Input()
  set isAdmin(value: boolean) {
    this._isAdmin = value;
  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }

  @Input()
  set user(user: User) {
    this._user = user;

    if (user != null) {
      this._userForm.controls.firstName.setValue(user.firstName);
      this._userForm.controls.lastName.setValue(user.lastName);
      this._userForm.controls.mail.setValue(user.email);
      this._userForm.controls.phoneNumber.setValue(user.phoneNumber);
      this._userForm.controls.postalCode.setValue(user.postalCode);
      this._userForm.controls.security.setValue(user.securityLevel);
      this._userForm.controls.balance.setValue(user.balance);
      this._modifying = true;
    }
  }

  get modifying(): boolean {
    return this._modifying;
  }

  get user(): User {
    return this._user;
  }
}
