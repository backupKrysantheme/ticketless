import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './user';
import {UserSessionService} from './user-session.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public static readonly URL_API_USER: string = '/api/user';

  constructor(public http: HttpClient) {
  }

  public query(): Observable<User[]> {
    return this.http.get<User[]>(UserService.URL_API_USER);
  }

  public get(id: number): Observable<User> {
    return this.http.get<User>(UserService.URL_API_USER + '/' + id);
  }

  public post(user: User): Observable<User> {
    return this.http.post<User>(UserService.URL_API_USER, user.toJson());
  }

  public update(user: User): Observable<User> {
    return this.http.put<User>(UserService.URL_API_USER, user.toJson());
  }

  public delete(user: User): Observable<any> {
    return this.http.delete<any>(UserService.URL_API_USER + '/' + user.id);
  }
}
