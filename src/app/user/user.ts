import {Merch} from '../merch/merch';

export class User {
  private _id: number;
  private _firstName: string;
  private _lastName: string;
  private _email: string;
  private _phoneNumber: string;
  private _postalCode: string;
  private _securityLevel: number;
  private _balance: number;
  private _passwd: string;
  private _qrcode: string;
  private _token: string;

  private _cart: Merch[] = [];

  constructor(firstName: string = '', lastName: string = '', email: string = '', phoneNumber: string = '', postalCode: string = '', securityLevel: number = 0, balance: number = 0,
              passwd: string = '', qrcode: string = '', token = '') {
    this._firstName = firstName;
    this._lastName = lastName;
    this._email = email;
    this._phoneNumber = phoneNumber;
    this._postalCode = postalCode;
    this._securityLevel = securityLevel;
    this._balance = balance;
    this._passwd = passwd;
    this._qrcode = qrcode;
    this._token = token;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get phoneNumber(): string {
    return this._phoneNumber;
  }

  set phoneNumber(value: string) {
    this._phoneNumber = value;
  }

  get postalCode(): string {
    return this._postalCode;
  }

  set postalCode(value: string) {
    this._postalCode = value;
  }

  get securityLevel(): number {
    return this._securityLevel;
  }

  set securityLevel(value: number) {
    this._securityLevel = value;
  }

  get balance(): number {
    return this._balance;
  }

  set balance(value: number) {
    this._balance = value;
  }

  public fromJson(json: any): User {
    Object.assign(this, json);
    this._cart = this._cart.map(merch => new Merch().fromJson(merch));
    return this;
  }

  get passwd(): string {
    return this._passwd;
  }

  set passwd(value: string) {
    this._passwd = value;
  }

  get qrcode(): string {
    return this._qrcode;
  }

  set qrcode(value: string) {
    this._qrcode = value;
  }

  public toJson(): any {
    return {
      'id': this._id,
      'firstName': this._firstName,
      'lastName': this._lastName,
      'email': this._email,
      'phoneNumber': this._phoneNumber,
      'postalCode': this._postalCode,
      'securityLevel': this._securityLevel,
      'balance': this._balance,
      'passwd': this._passwd,
      'qrcode': this._qrcode,
      'token': this._token,
      'cart': this._cart.map(merch => merch.toJson())
    };
  }

  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }

  public addMerch(merch: Merch) {
    this._cart.push(merch);
  }

  get cart(): Merch[] {
    return this._cart;
  }

  public clearCart() {
    this._cart = [];
  }

  public removeMerchFromCart(merchToRemove:Merch) {
    this._cart = this._cart.filter(merch => merch.id != merchToRemove.id);
  }

  set cart(value: Merch[]) {
    this._cart = value;
  }
}
