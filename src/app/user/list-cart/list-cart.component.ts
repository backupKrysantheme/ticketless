import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Merch} from '../../merch/merch';

@Component({
  selector: 'app-list-cart',
  templateUrl: './list-cart.component.html',
  styleUrls: ['./list-cart.component.css']
})
export class ListCartComponent implements OnInit {
  private _cart: Merch[] = [];
  private _amounts: number[] = [];
  private _onChangeAmount: EventEmitter<number[]> = new EventEmitter();
  private _onDeleteCartItem: EventEmitter<Merch> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  @Output()
  get onChangeAmount(): EventEmitter<number[]> {
    return this._onChangeAmount;
  }

  @Output()
  get onDeleteCartItem(): EventEmitter<Merch> {
    return this._onDeleteCartItem;
  }

  public changeAmount() {
    this._onChangeAmount.next(this._amounts);
  }

  public deleteCartItem(merch:Merch) {
    this._onDeleteCartItem.next(merch);
  }

  get cart(): Merch[] {
    return this._cart;
  }

  @Input()
  set cart(value: Merch[]) {
    this._cart = value;
  }

  get amounts(): number[] {
    return this._amounts;
  }

  @Input()
  set amounts(value: number[]) {
    this._amounts = value;
  }
}
