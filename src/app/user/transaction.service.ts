import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from './user';
import {Observable} from 'rxjs';
import {Merch} from '../merch/merch';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  public static readonly URL_API_TRANSACTION: string = '/api/transaction';

  constructor(public http: HttpClient) {
  }

  //Update the balance
  public update(user: User): Observable<User> {
    return this.http.put<User>(TransactionService.URL_API_TRANSACTION, user.toJson());
  }

  //Validate the buyout
  public post(user: User, merchs: Merch[], amounts: number[]): Observable<any> {
    let params = {
      'user': user.toJson(),
      'merchs': merchs.map(merch => merch.toJson()),
      'amounts': amounts
    };
    return this.http.post<any>(TransactionService.URL_API_TRANSACTION, params);
  }
}
