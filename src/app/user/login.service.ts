import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public static readonly URL_API_LOGIN: string = '/api/login';

  constructor(public http: HttpClient) {
  }

  public post(email: string, password: string, qrvalue: string): Observable<User> {
    let loginQuery = {
      'email': email,
      'password': password,
      'qrvalue': qrvalue
    };
    return this.http.post<User>(LoginService.URL_API_LOGIN, loginQuery);
  }

}
