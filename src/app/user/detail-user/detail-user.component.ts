import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {User} from '../user';
import {Subscription} from 'rxjs';
import {HistoryService} from '../../history/history.service';
import {History} from '../../history/history';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TransactionService} from '../transaction.service';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.css']
})
export class DetailUserComponent implements OnInit {

  private _user: User;
  private _histories: History[];
  private _chargeAccount: boolean;
  private _rechargeForm = new FormGroup({
    value: new FormControl('', Validators.compose([Validators.required, Validators.min(0), Validators.max(99999.99)])),
  });
  private _chargeAccountEvent: EventEmitter<number> = new EventEmitter();
  private _triggerUpdateAccountEvent: EventEmitter<boolean> = new EventEmitter();
  private _isAdmin:boolean;
  private _elementType : 'dataurl';

  constructor() {
  }

  ngOnInit() {
  }

  public rechargeAccount(cancel:boolean) {
    if(!cancel)
    {
      let val: number = this._rechargeForm.controls.value.value;
      this._chargeAccountEvent.next(val);
    }
    this.toggleChargeAccount();
  }

  public triggerUpdate() {
    let bool: boolean = true;
    this._triggerUpdateAccountEvent.next(bool);
  }

  get user(): User {
    return this._user;
  }

  @Input()
  set user(value: User) {
    this._user = value;
  }

  @Input()
  set histories(value: History[]) {
    this._histories = value;
  }

  get histories(): History[] {
    return this._histories;
  }

  public toggleChargeAccount() {
    this._chargeAccount = !this._chargeAccount;
  }

  get chargeAccount(): boolean {
    return this._chargeAccount;
  }

  get chargeForm(): FormGroup {
    return this._rechargeForm;
  }

  @Output()
  get onTriggerUpdateAccount(): EventEmitter<boolean> {
    return this._triggerUpdateAccountEvent;
  }

  @Output()
  get onChargeAccount(): EventEmitter<number> {
    return this._chargeAccountEvent;
  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }

  @Input()
  set isAdmin(value: boolean) {
    this._isAdmin = value;
  }

  get elementType(): "dataurl" {
    return this._elementType;
  }
}
