import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../user';
import {Router} from '@angular/router';
import {Lang_String} from '../../string';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  private _users: User[] = [];
  private _deleteUserEvent: EventEmitter<User> = new EventEmitter();
  private _modifyUserEvent: EventEmitter<User> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  get users(): User[] {
    return this._users;
  }

  public onDeleteClick(user: User) {
    if (confirm(Lang_String.CONFIRM_MESSAGE)) {
      this._deleteUserEvent.next(user);
    }
  }

  public onModifyClick(user: User) {
    this._modifyUserEvent.next(user);
  }

  @Input()
  set users(value: User[]) {
    this._users = value;
  }

  @Output()
  get onUserDeleted(): EventEmitter<User> {
    return this._deleteUserEvent;
  }

  @Output()
  get onUserModify(): EventEmitter<User> {
    return this._modifyUserEvent;
  }
}
