import {Injectable} from '@angular/core';
import {User} from './user';
import {LoginService} from './login.service';
import {Merch} from '../merch/merch';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  private _user: User;

  constructor(public loginService: LoginService) {
  }

  get user(): User {
    if (this._user == null) {
      this.loadFromLocalStorage();
    }

    return this._user;
  }

  set user(user: User) {
    this._user = user;
    //Save the user to the local storage
    this.saveSession();
  }

  public loadFromLocalStorage() {
    let userString: string;

    userString = localStorage.getItem('user');

    if (userString != null && userString != '') {
      this._user = new User().fromJson(JSON.parse(userString));

      let merchs: Merch[] = this._user.cart;
      this.loginService.post('', '', this._user.qrcode).subscribe(
        user => {
          this._user = new User().fromJson(user);
          this._user.cart = merchs;
          this.saveSession();
        }
      );
    }
  }

  public logout() {
    localStorage.removeItem('user');
    this._user = null;
  }

  public saveSession() {
    localStorage.setItem('user', JSON.stringify(this._user.toJson()));
  }
}
