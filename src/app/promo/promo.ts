export class Promo {
  private _id: number;
  private _code: string;
  private _percentage: number;
  private _expirationDate: string;

  constructor(code: string = '', percentage: number = 0, expirationDate: string = '') {
    this._code = code;
    this._percentage = percentage;
    this._expirationDate = expirationDate;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get code(): string {
    return this._code;
  }

  set code(value: string) {
    this._code = value;
  }

  get percentage(): number {
    return this._percentage;
  }

  set percentage(value: number) {
    this._percentage = value;
  }

  get expirationDate(): string {
    return this._expirationDate;
  }

  set expirationDate(value: string) {
    this._expirationDate = value;
  }

  public fromJson(json: any): Promo {
    Object.assign(this, json);

    return this;
  }

  public toJson(): any {
    return {
      'id': this._id,
      'code': this._code,
      'percentage': this._percentage,
      'expirationDate': this._expirationDate
    };
  }
}
