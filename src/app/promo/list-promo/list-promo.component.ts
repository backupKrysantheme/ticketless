import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Merch} from '../../merch/merch';
import {Promo} from '../promo';
import {User} from '../../user/user';
import {Lang_String} from '../../string';

@Component({
  selector: 'app-list-promo',
  templateUrl: './list-promo.component.html',
  styleUrls: ['./list-promo.component.css']
})
export class ListPromoComponent implements OnInit {
  private _promos: Promo[] = [];
  private _deletePromoEvent: EventEmitter<Promo> = new EventEmitter();
  private _modifyPromoEvent: EventEmitter<Promo> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  get promos(): Promo[] {
    return this._promos;
  }

  public onModifyClick(promo: Promo) {
    this._modifyPromoEvent.next(promo);
  }

  public onDeleteClick(promo: Promo) {
    if (confirm(Lang_String.CONFIRM_MESSAGE)) {
      this._deletePromoEvent.next(promo);
    }
  }

  @Input()
  set promos(value: Promo[]) {
    this._promos = value;
  }

  @Output()
  get onPromoDeleted(): EventEmitter<Promo> {
    return this._deletePromoEvent;
  }

  @Output()
  get onPromoModify(): EventEmitter<Promo> {
    return this._modifyPromoEvent;
  }

}
