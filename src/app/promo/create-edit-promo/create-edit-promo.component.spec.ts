import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditPromoComponent } from './create-edit-promo.component';

describe('CreateEditPromoComponent', () => {
  let component: CreateEditPromoComponent;
  let fixture: ComponentFixture<CreateEditPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
