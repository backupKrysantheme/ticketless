import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Promo} from '../promo';

@Component({
  selector: 'app-create-edit-promo',
  templateUrl: './create-edit-promo.component.html',
  styleUrls: ['./create-edit-promo.component.css']
})
export class CreateEditPromoComponent implements OnInit {

  private _promo: Promo;

  private _formPromo = new FormGroup({
      code: new FormControl('', Validators.required),
      percentage: new FormControl('', Validators.required),
      expire: new FormControl('', Validators.required),
    }
  );

  private _addPromoEvent: EventEmitter<Promo> = new EventEmitter();
  private _modifying: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  get formPromo(): FormGroup {
    return this._formPromo;
  }

  public onClickAddPromo(cancel: boolean) {
    if (!cancel) {
      if (!this._modifying) {
        this._promo = new Promo(
          this._formPromo.controls.code.value,
          this._formPromo.controls.percentage.value,
          this._formPromo.controls.expire.value,
        );
      } else {
        this._promo.code = this._formPromo.controls.code.value;
        this._promo.percentage = this._formPromo.controls.percentage.value;
        this._promo.expirationDate = this._formPromo.controls.expire.value;
      }
      this._addPromoEvent.next(this._promo);
    } else {
      this._addPromoEvent.next(null);
    }
  }

  @Input()
  set promo(promo: Promo) {
    this._promo = promo;

    if (promo != null) {
      this._formPromo.controls.code.setValue(promo.code);
      this._formPromo.controls.percentage.setValue(promo.percentage);
      this._formPromo.controls.expire.setValue(promo.expirationDate);
      this._modifying = true;
    }
  }

  get modifying(): boolean {
    return this._modifying;
  }

  @Output()
  get onPromoAdded(): EventEmitter<Promo> {
    return this._addPromoEvent;
  }

  get promo(): Promo {
    return this._promo;
  }
}
