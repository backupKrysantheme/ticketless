import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Promo} from './promo';
import {User} from '../user/user';
import {UserSessionService} from '../user/user-session.service';
import {Merch} from '../merch/merch';

@Injectable({
  providedIn: 'root'
})
export class PromoService {

  public static readonly URL_API_PROMO: string = '/api/promotion';

  constructor(public http: HttpClient) {
  }

  public query(): Observable<Promo[]> {
    return this.http.get<Promo[]>(PromoService.URL_API_PROMO);
  }

  public post(promo: Promo): Observable<Promo> {
    return this.http.post<Promo>(PromoService.URL_API_PROMO, promo.toJson());
  }

  public update(promo: Promo): Observable<Promo> {
    return this.http.put<Promo>(PromoService.URL_API_PROMO, promo.toJson());
  }

  public delete(promo: Promo): Observable<Promo> {
    return this.http.delete<Promo>(PromoService.URL_API_PROMO + '/' + promo.id);
  }

  public get(id: number): Observable<Promo> {
    return this.http.get<Promo>(PromoService.URL_API_PROMO + '/' + id);
  }
}
