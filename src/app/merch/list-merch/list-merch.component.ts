import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../user/user';
import {Merch} from '../merch';
import {Lang_String} from '../../string';

@Component({
  selector: 'app-list-merch',
  templateUrl: './list-merch.component.html',
  styleUrls: ['./list-merch.component.css']
})
export class ListMerchComponent implements OnInit {
  private _merchs: Merch[] = [];
  private _deleteMerchEvent: EventEmitter<Merch> = new EventEmitter();
  private _modifyMerchEvent: EventEmitter<Merch> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  get merchs(): Merch[] {
    return this._merchs;
  }

  @Input()
  set merchs(value: Merch[]) {
    this._merchs = value;
  }

  public onDeleteClick(merch: Merch) {
    if (confirm(Lang_String.CONFIRM_MESSAGE)) {
      this._deleteMerchEvent.next(merch);
    }
  }

  public onModifyClick(merch: Merch) {
    this._modifyMerchEvent.next(merch);
  }

  @Output()
  get onMerchDeleted(): EventEmitter<Merch> {
    return this._deleteMerchEvent;
  }

  @Output()
  get onMerchModify(): EventEmitter<Merch> {
    return this._modifyMerchEvent;
  }
}
