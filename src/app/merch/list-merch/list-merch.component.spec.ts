import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMerchComponent } from './list-merch.component';

describe('ListMerchComponent', () => {
  let component: ListMerchComponent;
  let fixture: ComponentFixture<ListMerchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMerchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMerchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
