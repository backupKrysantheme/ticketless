import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailMerchComponent } from './detail-merch.component';

describe('DetailMerchComponent', () => {
  let component: DetailMerchComponent;
  let fixture: ComponentFixture<DetailMerchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailMerchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailMerchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
