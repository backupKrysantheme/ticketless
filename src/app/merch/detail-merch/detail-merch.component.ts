import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Merch} from '../merch';

@Component({
  selector: 'app-detail-merch',
  templateUrl: './detail-merch.component.html',
  styleUrls: ['./detail-merch.component.css']
})
export class DetailMerchComponent implements OnInit {

  private _merch: Merch;
  private _addToCart: EventEmitter<Merch> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public onClick() {
    this._addToCart.next(this._merch);
  }

  get merch(): Merch {
    return this._merch;
  }

  @Input()
  set merch(value: Merch) {
    this._merch = value;
  }

  @Output()
  get addToCart(): EventEmitter<Merch> {
    return this._addToCart;
  }
}
