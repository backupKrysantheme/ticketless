import {Pipe, PipeTransform} from '@angular/core';
import {Merch} from './merch';

export enum filters {
  all = 0,
  fruit,
  vegetable,
  drink,
  cereals
}

@Pipe({
  name: 'filterMerch'
})
export class FilterMerchPipe implements PipeTransform {
  transform(merchs: Merch[], filter?: number): any {
    switch (filter) {
      case filters.all:
        return merchs;
      case filters.fruit:
        return merchs.filter(m => m.type == 'Fruit');
      case filters.vegetable:
        return merchs.filter(m => m.type == 'Vegetable');
      case filters.drink:
        return merchs.filter(m => m.type == 'Drink');
      case filters.cereals:
        return merchs.filter(m => m.type == 'Cereals');
    }

    return merchs;
  }

  public static intToNameOfOption(num: number): string {
    switch (num) {
      case filters.all:
        return 'Show all';
      case filters.fruit:
        return 'Show fruits';
      case filters.vegetable:
        return 'Show vegetables';
      case filters.drink:
        return 'Show drinks';
      case filters.cereals:
        return 'Show cereals';
    }
  }

  public static intToName(num: number): string {
    switch (num) {
      case filters.fruit:
        return 'Fruit';
      case filters.vegetable:
        return 'Vegetable';
      case filters.drink:
        return 'Drink';
      case filters.cereals:
        return 'Cereals';
    }
  }

  public static nameToInt(name: string): number {
    switch (name) {
      case 'Fruit':
        return filters.fruit;
      case 'Vegetable':
        return filters.vegetable;
      case 'Drink':
        return filters.drink;
      case 'Cereals':
        return filters.cereals;
    }
  }
}
