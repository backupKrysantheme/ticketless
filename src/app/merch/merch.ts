export class Merch {
  private _id: number;
  private _label: string;
  private _description: string;
  private _price: number;
  private _type: string;
  private _promoId: number;

  constructor(label: string = '', description: string = '', price: number = 0, type: string = '', promoId: number = -1) {
    this._label = label;
    this._description = description;
    this._price = price;
    this._type = type;
    this._promoId = promoId;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get label(): string {
    return this._label;
  }

  set label(value: string) {
    this._label = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get promoId(): number {
    return this._promoId;
  }

  set promoId(value: number) {
    this._promoId = value;
  }

  public fromJson(json: any): Merch {
    Object.assign(this, json);

    return this;
  }

  public toJson(): any {
    return {
      'id': this._id,
      'label': this._label,
      'description': this._description,
      'price': this._price,
      'type': this._type,
      'promoId': this._promoId
    };
  }

  public toString(): string {
    return '' + this._id + ' ' + this._label;
  }
}
