import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Merch} from './merch';
import {User} from '../user/user';
import {UserSessionService} from '../user/user-session.service';

@Injectable({
  providedIn: 'root'
})
export class MerchService {

  public static readonly URL_API_MERCH: string = '/api/merchandise';

  constructor(public http: HttpClient) {
  }

  public query(): Observable<Merch[]> {
    return this.http.get<Merch[]>(MerchService.URL_API_MERCH);
  }

  public post(merch: Merch): Observable<Merch> {
    return this.http.post<Merch>(MerchService.URL_API_MERCH, merch.toJson());
  }

  public update(merch: Merch): Observable<Merch> {
    return this.http.put<Merch>(MerchService.URL_API_MERCH, merch.toJson());
  }

  public delete(merch: Merch): Observable<any> {
    return this.http.delete<any>(MerchService.URL_API_MERCH + '/' + merch.id);
  }

  public get(id: number): Observable<Merch> {
    return this.http.get<Merch>(MerchService.URL_API_MERCH + '/' + id);
  }
}
