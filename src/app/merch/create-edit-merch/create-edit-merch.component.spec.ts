import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditMerchComponent } from './create-edit-merch.component';

describe('CreateEditMerchComponent', () => {
  let component: CreateEditMerchComponent;
  let fixture: ComponentFixture<CreateEditMerchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditMerchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditMerchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
