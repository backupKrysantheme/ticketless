import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Merch} from '../merch';
import {FilterMerchPipe, filters} from '../filter-merch.pipe';
import {isNumber} from 'util';

@Component({
  selector: 'app-create-edit-merch',
  templateUrl: './create-edit-merch.component.html',
  styleUrls: ['./create-edit-merch.component.css']
})
export class CreateEditMerchComponent implements OnInit {
  private _types: filters[] = Object.values(filters).filter(
    (type) => isNumber(<any>type)
  ).filter(value => value != 0);

  private _merch: Merch;

  private _formMerch = new FormGroup({
    label: new FormControl('', Validators.compose([Validators.required])),
    type: new FormControl('', Validators.compose([Validators.required])),
    price: new FormControl('', Validators.compose([Validators.required])),
    promoId: new FormControl('', Validators.compose([Validators.required])),
    description: new FormControl('', Validators.compose([Validators.required])),
  });

  private _addMerchEvent: EventEmitter<Merch> = new EventEmitter();
  private _modifying: boolean;

  constructor() {
  }

  @Output()
  get onMerchAdded(): EventEmitter<Merch> {
    return this._addMerchEvent;
  }

  public onClickAddMerch(cancel: boolean) {
    if (!cancel) {
      if (!this._modifying) {
        this._merch = new Merch(
          this._formMerch.controls.label.value,
          this._formMerch.controls.description.value,
          this._formMerch.controls.price.value,
          this.intToName(this._formMerch.controls.type.value),
          this._formMerch.controls.promoId.value
        );
      } else {
        this._merch.label = this._formMerch.controls.label.value;
        this._merch.description = this._formMerch.controls.description.value;
        this._merch.price = this._formMerch.controls.price.value;
        this._merch.type = this.intToName(this._formMerch.controls.type.value);
        this._merch.promoId = this._formMerch.controls.promoId.value;
      }
      this._addMerchEvent.next(this._merch);
    } else {
      this._addMerchEvent.next(null);
    }
  }

  ngOnInit() {
  }

  get types(): filters[] {
    return this._types;
  }

  intToName(num: number): string {
    return FilterMerchPipe.intToName(num);
  }

  get formMerch(): FormGroup {
    return this._formMerch;
  }

  @Input()
  set merch(merch: Merch) {
    this._merch = merch;

    if (merch != null) {
      this._formMerch.controls.label.setValue(merch.label);
      this._formMerch.controls.description.setValue(merch.description);
      this._formMerch.controls.price.setValue(merch.price);
      this._formMerch.controls.type.setValue(FilterMerchPipe.nameToInt(merch.type));
      this._formMerch.controls.promoId.setValue(merch.promoId);

      this._modifying = true;
    }
  }

  get modifying(): boolean {
    return this._modifying;
  }

  get merch(): Merch {
    return this._merch;
  }
}
