import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {UserSessionService} from '../user/user-session.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor(public userSession: UserSessionService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.userSession.user != null) {
      request = request.clone(
        {
          setHeaders: {
            Authorization: `Bearer ${this.userSession.user.token}`,
            'Content-Type': 'application/json'
          }
        });
    }
    return next.handle(request);
  }
}
