import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, OnDestroy, Output, Renderer2, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {interval} from 'rxjs';

@Component({
  selector: 'app-webcam-reader',
  templateUrl: './webcam-reader.component.html',
  styleUrls: ['./webcam-reader.component.css']
})

export class WebcamReaderComponent implements OnInit, AfterViewInit, OnDestroy {

  private _value = '';
  private _showQRCode = false;
  private _elementType = 'dataurl';
  private _verifyUser: EventEmitter<string> = new EventEmitter();
  private _captureSub: Subscription;

  @ViewChild('result')
  public resultElement: ElementRef;

  @ViewChild('video')
  public video: ElementRef;

  @ViewChild('canvas')
  public canvas: ElementRef;

  constructor(private renderer: Renderer2) {
  }

  public render(e: any) {
    this.renderer.createElement('p');
    if(e.result.toString() != null && e.result.toString() != "")
      this._verifyUser.next(e.result.toString());
  }

  public ngOnInit() {
    this._captureSub = interval(250)
      .subscribe(() => this.capture());
  }

  public ngOnDestroy() {
    if (this._captureSub != null)
      this._captureSub.unsubscribe();
      this.video.nativeElement.srcObject.getTracks()[0].stop();
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({video: true}).then(stream => {
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();
      });
    }
  }

  public capture() {
    this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
    this._value = this.canvas.nativeElement.toDataURL('image/png');
  }

  @Output()
  get qrValue(): EventEmitter<string> {
    return this._verifyUser;
  }

  get value(): string {
    return this._value;
  }

  get elementType(): string {
    return this._elementType;
  }

  get showQRCode(): boolean {
    return this._showQRCode;
  }
}
