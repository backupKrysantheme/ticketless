import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebcamReaderComponent } from './webcam-reader.component';

describe('WebcamReaderComponent', () => {
  let component: WebcamReaderComponent;
  let fixture: ComponentFixture<WebcamReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebcamReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcamReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
