import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {WebcamReaderComponent} from './camera/webcam-reader/webcam-reader.component';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './pages/index/index.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ListUserComponent} from './user/list-user/list-user.component';
import {ListMerchComponent} from './merch/list-merch/list-merch.component';
import {ListPromoComponent} from './promo/list-promo/list-promo.component';
import {ListHistoryComponent} from './history/list-history/list-history.component';
import {NgQRCodeReaderModule} from 'ng2-qrcode-reader';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {RegisterComponent} from './pages/register/register.component';
import {LoginComponent} from './pages/login/login.component';
import {AdminComponent} from './pages/admin/admin.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatTableModule, MatTabsModule} from '@angular/material';
import {DetailUserComponent} from './user/detail-user/detail-user.component';
import {DetailUserSmartComponent} from './pages/userDetails/detail-user-smart.component';
import {NavbarComponent} from './navigation/navbar/navbar.component';
import {CreateEditUserComponent} from './user/create-edit-user/create-edit-user.component';
import {DetailMerchSmartComponent} from './pages/merchDetails/detail-merch-smart.component';
import {DetailMerchComponent} from './merch/detail-merch/detail-merch.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptorService} from './http/http-interceptor.service';
import {CheckoutComponent} from './pages/checkout/checkout.component';
import {ListCartComponent} from './user/list-cart/list-cart.component';
import {MerchComponent} from './pages/merchList/merch.component';
import {ToastrModule} from 'ngx-toastr';
import {CreateEditMerchComponent} from './merch/create-edit-merch/create-edit-merch.component';
import {FilterMerchPipe} from './merch/filter-merch.pipe';
import {CreateEditPromoComponent} from './promo/create-edit-promo/create-edit-promo.component';
import {ConfirmationInscriptionComponent} from './pages/confirmation-inscription/confirmation-inscription.component';
import {ListMerchClientComponent} from './pages/list-merch-client/list-merch-client.component';

const routes: Routes = [
  {path: 'camera', component: WebcamReaderComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'confirmationSignUp', component: ConfirmationInscriptionComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'user/:id', component: DetailUserSmartComponent},
  {path: 'merch/:id', component: DetailMerchSmartComponent},
  {path: 'merch', component: MerchComponent},
  {path: 'checkout', component: CheckoutComponent},
  {path: '', component: IndexComponent, pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    WebcamReaderComponent,
    IndexComponent,
    ListUserComponent,
    ListMerchComponent,
    ListPromoComponent,
    ListHistoryComponent,
    RegisterComponent,
    ConfirmationInscriptionComponent,
    LoginComponent,
    AdminComponent,
    DetailUserComponent,
    DetailUserSmartComponent,
    NavbarComponent,
    CreateEditUserComponent,
    DetailMerchSmartComponent,
    DetailMerchComponent,
    ListMerchClientComponent,
    CheckoutComponent,
    ListCartComponent,
    MerchComponent,
    CreateEditMerchComponent,
    FilterMerchPipe,
    CreateEditPromoComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    NgQRCodeReaderModule,
    NgxQRCodeModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTableModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
