export class History {
  private _id: number;
  private _merchId: number;
  private _userId: number;
  private _date: string;
  private _amount: number;

  constructor(merchId: number = 0, userId: number = 0, date: string = '', amount: number = 0) {
    this._merchId = merchId;
    this._userId = userId;
    this._date = date;
    this._amount = amount;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get merchId(): number {
    return this._merchId;
  }

  set merchId(value: number) {
    this._merchId = value;
  }

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }

  get date(): string {
    return this._date;
  }

  set date(value: string) {
    this._date = value;
  }

  get amount(): number {
    return this._amount;
  }

  set amount(value: number) {
    this._amount = value;
  }

  public fromJson(json: any): History {
    Object.assign(this, json);

    return this;
  }

  public toJson(): any {
    return {
      'id': this._id,
      'merchId': this._merchId,
      'userId': this._userId,
      'date': this._date,
      'amount': this._amount
    };
  }
}
