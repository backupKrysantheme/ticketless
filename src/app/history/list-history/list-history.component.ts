import {Component, Input, OnInit} from '@angular/core';
import {History} from '../history';

@Component({
  selector: 'app-list-history',
  templateUrl: './list-history.component.html',
  styleUrls: ['./list-history.component.css']
})
export class ListHistoryComponent implements OnInit {
  private _histories: History[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  get histories(): History[] {
    return this._histories;
  }

  @Input()
  set histories(value: History[]) {
    this._histories = value;
  }
}
