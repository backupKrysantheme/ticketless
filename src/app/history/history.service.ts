import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {History} from './history';
import {Promo} from '../promo/promo';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  public static readonly URL_API_HISTORY:string ="/api/history";

  constructor(public http: HttpClient) { }

  public query() : Observable<History[]>
  {
    return this.http.get<History[]>(HistoryService.URL_API_HISTORY);
  }

  public post(history:History) : Observable<History>
  {
    return this.http.post<History>(HistoryService.URL_API_HISTORY,history.toJson());
  }

  public update(history:History) : Observable<History>
  {
    return this.http.put<History>(HistoryService.URL_API_HISTORY,history.toJson());
  }

  public delete(history:History) : Observable<History>
  {
    return this.http.delete<History>(HistoryService.URL_API_HISTORY + '/' + history.id);
  }

  public get(id:number) : Observable<History[]>
  {
    return this.http.get<History[]>(HistoryService.URL_API_HISTORY + '/' + id);
  }
}
