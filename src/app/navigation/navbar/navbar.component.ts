import {AfterContentInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from '../../user/user';
import {UserSessionService} from '../../user/user-session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public userService: UserSessionService) {
  }

  private _activeUser: User;

  public gotoUserDetails(): string {
    return '/user/' + this._activeUser.id;
  }

  public gotoAdminMenu(): string {
    return '/admin';
  }

  public getUser() {
    this._activeUser = this.userService.user;
  }

  public getActiveUserName(): string {
    this.getUser();
    if (!this.isUserNull()) {
      return '' + this._activeUser.firstName;
    } else {
      return '';
    }
  }

  public isUserNull(): boolean {
    return (this._activeUser == null || this._activeUser.firstName == '');
  }

  public isAdmin(): boolean {
    return (this._activeUser.securityLevel == 1);
  }

  public logout() {
    this.userService.logout();
    this._activeUser = null;
  }

  ngOnInit() {
    this.getUser();
  }
}
