import { TestBed } from '@angular/core/testing';

import { PasswordMakerService } from './password-maker.service';

describe('PasswordMakerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PasswordMakerService = TestBed.get(PasswordMakerService);
    expect(service).toBeTruthy();
  });
});
