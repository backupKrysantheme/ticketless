import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PasswordMakerService {

  public static readonly  URL_API_PASSWORDMAKER:string = "https://makemeapassword.ligos.net/api/v1/passphrase/json?";
  public static readonly  URL_API_PC:string = "pc=1";
  public static readonly  URL_API_WC:string = "&wc=12";
  public static readonly  URL_API_SP:string = "&sp=n";
  public static readonly  URL_API_MAXCH:string = "&maxCh=200";




  constructor(public http: HttpClient){}

  public get() : Observable<String>
  {
    let subRequest:string = PasswordMakerService.URL_API_PASSWORDMAKER+PasswordMakerService.URL_API_PC+PasswordMakerService.URL_API_WC+PasswordMakerService.URL_API_SP+PasswordMakerService.URL_API_MAXCH;
    return this.http.get<String>(subRequest);
  }

}
